/*
 * Line.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.Graphics2D;

// Class which represents a line connecting two countries(nodes).
public class Line {
	private int x1;
	private int y1;
	private int x2;
	private int y2;

	public Line(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public void draw(Graphics2D g2d) {
		g2d.drawLine(getX1Center(), getY1Center(), getX2Center(), getY2Center());
	}

	public int getX1() {
		return x1;
	}

	public int getY1() {
		return y1;
	}

	public int getX2() {
		return x2;
	}

	public int getY2() {
		return y2;
	}

	public int getX1Center() {
		return x1 + Constants.NODE_HEIGHT / 2;
	}

	public int getY1Center() {
		return y1 + Constants.NODE_HEIGHT / 2;
	}

	public int getX2Center() {
		return x2 + Constants.NODE_HEIGHT / 2;
	}

	public int getY2Center() {
		return y2 + Constants.NODE_HEIGHT / 2;
	}
}
