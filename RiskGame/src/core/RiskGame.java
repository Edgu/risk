/*
 * RiskGame.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class RiskGame extends JPanel {
	private Node[] nodes;
	private List<Line> lines;
	private Player[] players;
	private Console console;
	private Deck deck;

	private static final int NUMBER_OF_UNITS_FOR_NEUTRAL = 24;
	private static final int NUMBER_OF_UNITS_FOR_PLAYER = 36;

	public RiskGame(Console console) {
		this.console = console;
		this.setUpBorder(Color.BLUE);
		this.setBackground(Color.BLACK);
		this.add(setMap("/risk_game_map.jpg"));
		this.addMouseMotionListener(new CustomMouseMotionListener(this));

		nodes = new Node[Constants.COUNTRY_COORD.length];
		lines = new ArrayList<Line>();
		players = new Player[Constants.NUM_PLAYERS_PLUS_NEUTRALS];
		deck = new Deck();

		// Create nodes
		for (int i = 0; i < Constants.NUM_COUNTRIES; i++) {
			nodes[i] = new Node(Constants.COUNTRY_NAMES[i], Constants.ADJACENT[i], Constants.COUNTRY_COORD[i],
					Constants.CONTINENT_IDS[i], Constants.getContinentColor(i), null);
		}

		// Create Players
		for (int i = 0; i < Constants.NUM_PLAYERS_PLUS_NEUTRALS; i++) {
			// Check if player is passive or not.
			boolean passivePlayer = (i > 1) ? true : false;
			// Decide number of units player will have depending if the player
			// is neutral or not.
			int numberOfUnits = (passivePlayer) ? NUMBER_OF_UNITS_FOR_NEUTRAL : NUMBER_OF_UNITS_FOR_PLAYER;

			players[i] = new Player("Player" + i, numberOfUnits, null, passivePlayer);

			// Draw cards and assign countries to the player
			for (int j = 0; j < (passivePlayer ? Constants.INIT_COUNTRIES_NEUTRAL
					: Constants.INIT_COUNTRIES_PLAYER); j++) {
				String country = deck.pop().getTerritory();
				int index = Constants.getCountryId(country);

				players[i].addCountryOwned(nodes[index]);
				nodes[index].setOwner(players[i]);
				players[i].setAmountOfUnits(players[i].getAmountOfUnits() - 1);
				nodes[index].setUnits(1);
			}
		}
		deck.reset();
		deck.addWildCards();

		// Create lines connecting nodes
		for (int i = 0; i < nodes.length; i++) {
			for (int j = 0; j < nodes[i].countAdjacentNodes(); j++) {
				int oneAdjacentNode = nodes[i].getAdjacentNodes()[j];
				// Avoiding drawing two lines on top of each other
				if (i < oneAdjacentNode) {
					// Connecting Alaska and Kamchatka
					if (i == 8 && oneAdjacentNode == 22) {
						lines.add(new Line(nodes[i].getX(), nodes[i].getY(), 10 - Constants.NODE_WIDTH / 2,
								nodes[i].getY()));
						lines.add(new Line(nodes[oneAdjacentNode].getX(), nodes[oneAdjacentNode].getY(),
								Constants.FRAME_WIDTH - 13, nodes[oneAdjacentNode].getY()));
						continue;
					}
					lines.add(new Line(nodes[i].getX(), nodes[i].getY(), nodes[oneAdjacentNode].getX(),
							nodes[oneAdjacentNode].getY()));
				}
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		// Enable Anti-Aliasing
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setStroke(new BasicStroke(2));
		g2d.setFont(new Font(g2d.getFont().getFontName(), Font.BOLD, 11));

		// Draw lines interconnecting nodes.
		for (Line line : lines) {
			line.draw(g2d);
		}

		// Draw nodes.
		for (Node node : nodes) {
			node.draw(g2d);
		}

		// Information displaying colors and players who have those colors
		int startingYPlayers = 350;
		for (int i = 0; i < Constants.NUM_PLAYERS_PLUS_NEUTRALS; i++) {
			int startingXPlayers = 20;
			g2d.setColor(players[i].getColour());
			g2d.fillOval(startingXPlayers, startingYPlayers, Constants.NODE_HEIGHT, Constants.NODE_WIDTH);
			g2d.setColor(Color.BLACK);
			g2d.drawOval(startingXPlayers - 1, startingYPlayers - 1, Constants.NODE_HEIGHT + 1,
					Constants.NODE_WIDTH + 1);
			String name = players[i].getName();
			int textHeight = g2d.getFontMetrics().getAscent();

			// Draw outline of the players name.
			g2d.setColor(Color.WHITE);
			g2d.drawString(name, startingXPlayers + Constants.NODE_HEIGHT + 10 - 1,
					startingYPlayers + Constants.NODE_HEIGHT / 2 + textHeight / 2 - 1);
			g2d.drawString(name, startingXPlayers + Constants.NODE_HEIGHT + 10 + 1,
					startingYPlayers + Constants.NODE_HEIGHT / 2 + textHeight / 2 - 1);
			g2d.drawString(name, startingXPlayers + Constants.NODE_HEIGHT + 10 - 1,
					startingYPlayers + Constants.NODE_HEIGHT / 2 + textHeight / 2 + 1);
			g2d.drawString(name, startingXPlayers + Constants.NODE_HEIGHT + 10 + 1,
					startingYPlayers + Constants.NODE_HEIGHT / 2 + textHeight / 2 + 1);

			// Draw player name
			g2d.setColor(Color.BLACK);
			g2d.drawString(name, startingXPlayers + Constants.NODE_HEIGHT + 10,
					startingYPlayers + Constants.NODE_HEIGHT / 2 + textHeight / 2);

			startingYPlayers += 40;
		}
	}

	public Player[] getPlayers() {
		return players;
	}

	public Node getNode(int i) {
		return nodes[i];
	}

	class CustomMouseMotionListener implements MouseMotionListener {
		private RiskGame risk;

		public CustomMouseMotionListener(RiskGame risk) {
			this.risk = risk;
			risk.setToolTipText(null);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			int index = Constants.getCountry(e);

			if (index != -1) {
				int units = risk.getNode(index).getUnits();
				String owner = "";
				if (risk.getNode(index).getOwner() != null) {
					owner = risk.getNode(index).getOwner().getName();
				}
				risk.setToolTipText("<html>Country: " + Constants.COUNTRY_NAMES[index] + "<br>Short name: "
						+ Constants.COUNTRY_SHORT_NAMES[index] + "<br>Owner: " + owner + "<br>Units: " + units
						+ "</html>");
			} else {
				risk.setToolTipText(null);
			}
		}
	}

	private JLabel setMap(String map) {
		URL url = RiskGame.class.getResource(map);
		ImageIcon icon = new ImageIcon(url);
		Image image = icon.getImage();
		image = image.getScaledInstance(Constants.FRAME_WIDTH - Constants.BORDER_OFFSET * 2,
				Constants.FRAME_HEIGHT - Constants.BORDER_OFFSET * 4, Image.SCALE_SMOOTH);
		icon = new ImageIcon(image);

		return new JLabel(icon);
	}

	private void setUpBorder(Color colour) {
		this.setBorder(BorderFactory.createLineBorder(colour, 5));
	}

	private int getWinner() {
		int winnerID = -1;

		for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
			if (!players[i].getCountriesOwned().isEmpty()) {
				if (winnerID == -1) {
					winnerID = i;
				} else {
					// 1 other person has countries left so no winner
					return -1;
				}
			}
		}
		return winnerID;
	}

	public int startingPlayer() {
		int playerOneDice, playerTwoDice;

		do {
			playerOneDice = players[0].rollDice();
			Console.appendText("Player 1 rolled: " + playerOneDice + "\n");
			playerTwoDice = players[1].rollDice();
			Console.appendText("Player 2 rolled: " + playerTwoDice + "\n");
			if (playerOneDice == playerTwoDice) {
				Console.appendText("Rerolling because both players rolled the same number.\n");
			}
		} while (playerOneDice == playerTwoDice);

		return (playerOneDice > playerTwoDice) ? 0 : 1;
	}

	public int placeStartingUnits() {
		List<Player> players = new ArrayList<Player>(Arrays.asList(getPlayers()));
		int index = startingPlayer();

		// Display which player starts first.
		Console.appendText(players.get(index).getName() + " starts placing units first.\n");

		while (!players.isEmpty()) {
			if (players.get(index).getAmountOfUnits() > 0) {
				if (!players.get(index).isPassive()) { // if player is not
					// neutral.
					String countryName = console.askUser(players.get(index).getName()
							+ " please enter country name in which you wish to place 3 units: \n");
					int countryId = Constants.getCountryId(countryName);
					// Player either entered wrong name or does not own the
					// country.
					while (countryId == -1 || (nodes[countryId].getOwner() != null
							&& nodes[countryId].getOwner() != players.get(index))) {
						if (countryId == -1) {
							Console.appendText("This country does not exist\n");
						} else if (nodes[countryId].getOwner() != null
								&& nodes[countryId].getOwner() != players.get(index)) {
							Console.appendText("This country is already owned or you did not draw it\n");
						}
						countryName = console.askUser(players.get(index).getName()
								+ " please enter country name in which you wish to place 3 units: \n");
						countryId = Constants.getCountryId(countryName);
					}

					placeUnits(nodes[countryId], players.get(index), 3);
				} else { // is player is neutral.
					// If neutral player has less than INIT_COUNTRIES_NEUTRAL
					// countries. (< 6)
					// Set units only in countries which are not owned by any
					// player.
					if (players.get(index).getCountriesOwned().size() < Constants.INIT_COUNTRIES_NEUTRAL) {
						for (int i = 0; i < Constants.NUM_COUNTRIES; i++) {
							if (nodes[i].getOwner() != null) {
								continue;
							}

							if (placeUnits(nodes[i], players.get(index), 1)) {
								break;
							}
						}
					} else { // Get random country from the list and add units.
						int randomCountry = (int) (Math.random() * players.get(index).getCountriesOwned().size());
						placeUnits(players.get(index).getCountriesOwned().get(randomCountry), players.get(index), 1);
					}
				}
			} else {
				players.remove(index);
			}

			refreshMap();
			index = (index >= (players.size() - 1)) ? 0 : index + 1;
		}
		return index;
	}

	public boolean placeUnits(Node node, Player player, int units) {
		if (units <= 0 || node.getOwner() != null && node.getOwner() != player) {
			return false;
		}

		if (node.getOwner() == null) {
			node.setOwner(player);
			player.getCountriesOwned().add(node);
		}

		node.setUnits(node.getUnits() + units);
		player.setAmountOfUnits(player.getAmountOfUnits() - units);

		refreshMap();
		return true;
	}

	public void combat(Player player) {
		if (!player.isPassive()) {
			while (requestInput("Do you want to attack or skip?\n<attack, skip>", "attack", "skip")) {
				String country1 = console.askUser("From which country you want to attack? \n\n");
				int country1Id = Constants.getCountryId(country1);

				// ********** ERROR CHECKING **********

				// In case player tries to use non-existent country
				while (country1Id == -1) {
					Console.appendText(Constants.countryDoesNotExist);
					country1 = console.askUser("From which country you want to attack? \n\n");
					country1Id = Constants.getCountryId(country1);
				}

				// In case player tries to use not his country
				while (!nodes[country1Id].getOwner().equals(player)) {
					Console.appendText(Constants.notCountryOwner);
					country1 = console.askUser("From which country you want to attack? \n\n");
					country1Id = Constants.getCountryId(country1);
				}

				// In case player tries to attack not adjacent country
				boolean adjacentOrNot = false;
				String country2 = null;
				int country2Id = 0;

				while (!adjacentOrNot) {
					country2 = console.askUser("Which country you want to attack?\n\n");
					country2Id = Constants.getCountryId(country2);
					adjacentOrNot = nodes[country1Id].isAdjacent(country2Id);

					if (!adjacentOrNot) {
						Console.appendText("You tried to attack not an adjacent country\n\n");
					}
				}

				// In case player tries to attack his own country
				while (nodes[country2Id].getOwner().equals(player)) {
					Console.appendText("You are trying to attack your own country!\n\n");
					country2 = console.askUser("Say again, which country you want to attack?\n\n");
					country2Id = Constants.getCountryId(country2);
				}

				int attUnits = Integer.parseInt(console
						.askUser(player.getName() + ", enter the amount of units you want to use in attack\n\n"));
				Player player2 = nodes[country2Id].getOwner();

				// In case player tries to use more than 3 units in attack
				while (attUnits > 3) {
					Console.appendText("You cannot attack with more than 3 units\n\n");
					attUnits = Integer
							.parseInt(console.askUser("Enter again the amount of units you want to use in attack\n\n"));
				}

				// In case player tries to use all units in attack
				while (nodes[country1Id].getUnits() - attUnits < 1) {
					Console.appendText("You have to leave at least one unit in the territory!\n\n");
					attUnits = Integer
							.parseInt(console.askUser("Enter again the amount of units you want to use in attack\n\n"));
				}

				int defUnits = 0;

				if (player2.isPassive()) {
					defUnits = (nodes[country2Id].getUnits() > 1) ? 2 : 1;
					Console.appendText(
							"Neutral player " + player2.getName() + " is using " + defUnits + " units in defense!\n");
				} else {
					defUnits = Integer.parseInt(console
							.askUser(player2.getName() + ", enter the amount of units you want to use in defence\n\n"));
				}

				// In case player tries to use more units while defending than
				// he
				// actually has
				while (defUnits > nodes[country2Id].getUnits()) {
					Console.appendText("You do not have " + defUnits + " in this terittory!\n\n");
					defUnits = Integer.parseInt(
							console.askUser("Enter again the amount of units you want to use in defence\n\n"));
				}

				// In case player tries to defend not with 1 or 2 units
				while (defUnits != 1 && defUnits != 2) {
					Console.appendText("You have to use 1 or 2 units in defence\n\n");
					defUnits = Integer.parseInt(
							console.askUser("Enter again the amount of units you want to use in defence\n\n"));
				}

				// ********** MAJOR PART **********
				ArrayList<Integer> attackerUnits = new ArrayList<Integer>();
				ArrayList<Integer> defenderUnits = new ArrayList<Integer>();

				// Rolling dice for attacker and defender
				for (int i = 0; i < attUnits; i++) {
					attackerUnits.add(player.rollDice());
				}

				for (int i = 0; i < defUnits; i++) {
					defenderUnits.add(player2.rollDice());
				}

				// Sorting lists
				Collections.sort(attackerUnits);
				Collections.reverse(attackerUnits);
				Collections.sort(defenderUnits);
				Collections.reverse(defenderUnits);

				int counter = 1;
				do {
					int winner = 0;
					int attacker = attackerUnits.get(0);
					int defender = defenderUnits.get(0);

					Console.appendText(player.getName() + "'s " + counter + " highest roll is " + attacker + "\n");
					Console.appendText(player2.getName() + "'s " + counter + " highest roll is " + defender + "\n");

					if (attacker > defender) {
						nodes[country1Id].setUnits(nodes[country1Id].getUnits() - 1);
						nodes[country2Id].setUnits(nodes[country2Id].getUnits() - 1);
						attackerUnits.remove(0);
						defenderUnits.remove(0);
						winner++;
						Console.appendText(player.getName() + " wins this roll\n\n");
					} else {
						nodes[country1Id].setUnits(nodes[country1Id].getUnits() - 1);
						winner--;
						attackerUnits.remove(0);
						defenderUnits.remove(0);
						Console.appendText(player2.getName() + " wins this roll\n\n");
					}

					if (nodes[country2Id].getUnits() == 0) {
						nodes[country2Id].setOwner(player);
						nodes[country2Id].setUnits(winner);
						player2.removeCountryOwned(nodes[country2Id]);
						player.addCountryOwned(nodes[country2Id]);
						Console.appendText(
								"Congratulations, " + player.getName() + ", you have conquered this country!\n");
						Card newCard = deck.pop();
						if (!deck.isEmpty()) {
							player.addCard(newCard);
						}
						Console.appendText("This is your new card: " + newCard);

						// If player2 is completely defeated, then transfer all
						// cards to player
						if (player2.getCountriesOwned().isEmpty()) {
							while (player2.numCards() > 0) {
								player.addCard(player2.removeCard());
							}
						}
						break;
					}
					counter++;
				} while (!attackerUnits.isEmpty() && !defenderUnits.isEmpty());

				Console.appendText("Battle finished succesfully\n\n");

				refreshMap();
			}
		}
	}

	public int exchangeCards(Player player) {
		int reinforcements = 0;
		while (player.numCards() > 4) {
			Console.appendText("As you have more than 4 cards, you must exchange cards\n");
			String setToBeExchanged = null;
			char[] letters = new char[3];
			boolean unique;

			setToBeExchanged = console.askUser("Enter the set you wish to exchange\n").toUpperCase();

			// Checking if correct number of letters has been entered
			if (setToBeExchanged.length() != 3) {
				Console.appendText("Set has to contain exactly 3 cards!\n");
				continue;
			}

			// Checking if correct letters have been entered
			while (true) {
				letters[0] = setToBeExchanged.charAt(0);
				letters[1] = setToBeExchanged.charAt(1);
				letters[2] = setToBeExchanged.charAt(2);

				for (int i = 0; i < letters.length; i++) {
					switch (letters[i]) {
					case 'A':
					case 'C':
					case 'I':
						continue;
					default:
						Console.appendText("Wrong set of insignias!\n");
						setToBeExchanged = console.askUser("Enter the set you wish to exchange again\n").toUpperCase();
						i = 0;
						break;
					}
				}
				break;
			}

			// Checking if the set is three same letters or all different
			// letters
			while (true) {
				letters[0] = setToBeExchanged.charAt(0);
				letters[1] = setToBeExchanged.charAt(1);
				letters[2] = setToBeExchanged.charAt(2);

				if (!(letters[0] == letters[1] && letters[1] == letters[2])) {
					if (!(letters[0] != letters[1] && letters[1] != letters[2])) {
						Console.appendText("You entered wrong combination of letters\n");
						setToBeExchanged = console.askUser("Enter the set you wish to exchange again\n").toUpperCase();
						continue;
					} else {
						unique = false;
						break;
					}
				} else {
					unique = true;
					break;
				}
			}
			reinforcements += player.getReinforcementsForCards(setToBeExchanged, unique);
		}
		return reinforcements;
	}

	public void placeReinforcements(Player player) {
		int reinforcements = 0;

		if (player.numCards() > 0) {
			Console.appendText(player.getName() + ", " + player.printCards());
		}

		reinforcements = exchangeCards(player);

		reinforcements += player.getTotalReinforcements();
		player.setAmountOfUnits(reinforcements);
		Console.appendText(player.getName() + " got " + reinforcements + " reinforcements\n\n");

		if (!player.isPassive()) {
			while (player.getAmountOfUnits() > 0) {
				String countryName = console.askUser("Where do you want to put your reinforcements? \n");
				int countryId = Constants.getCountryId(countryName);

				// In case player tries to put reinforcements in non-existent
				// country
				while (countryId == -1) {
					Console.appendText(Constants.countryDoesNotExist);
					countryName = console.askUser("Enter again where you want to put your reinforcements? \n\n");
					countryId = Constants.getCountryId(countryName);
				}

				// In case player tries to put reinforcements in other player's
				// country
				while (nodes[countryId].getOwner() != player || countryId == -1) {
					Console.appendText(Constants.notCountryOwner);
					countryName = console.askUser("Enter again where you want to put your reinforcements? \n\n");
					countryId = Constants.getCountryId(countryName);
				}

				int numberOfUnits = Integer.parseInt(console.askUser("How many units you want to put there? \n\n"));

				// In case player tries to put too many units
				while (player.getAmountOfUnits() - numberOfUnits < 0) {
					Console.appendText("You do not have " + numberOfUnits + " units left in your reinforcements\n\n");
					numberOfUnits = Integer
							.parseInt(console.askUser("Please say again, how many units you want to put there? \n\n"));
				}

				if (placeUnits(nodes[countryId], player, numberOfUnits)) {
					Console.appendText("Units placed succesfully\n\n");
				}
				Console.appendText(player.getName() + " has finished placing reinforcements\n\n");
			}
			// Placing reinforcements automatically for neutral players
		} else {
			Node[] randomCountries = new Node[reinforcements];
			for (int i = 0; i < reinforcements; i++) {
				randomCountries[i] = player.getRandomCountry();
				if (placeUnits(randomCountries[i], player, 1)) {
					Console.appendText("Units placed succesfully\n\n");
				}
			}
			Console.appendText(player.getName() + " has finished placing reinforcements\n\n");
		}
		return;
	}

	public void fortifyTerritory(Player player) {
		if (!player.isPassive()) {
			String movingFrom = "";
			String movingTo = "";
			int movingFromId = 0;
			int movingToId = 0;
			int numUnitsToMove = 0;

			while (requestInput("Do you want to fortify a country or skip?\n<fortify, skip>", "fortify", "skip")) {
				// Get country from which units will be moved.
				while (true) {
					movingFrom = console.askUser("From which country do you want to move units?\n\n");
					movingFromId = Constants.getCountryId(movingFrom);

					if (movingFromId == -1) {
						Console.appendText(Constants.countryDoesNotExist);
						continue;
					} else if (nodes[movingFromId].getOwner() != player) {
						Console.appendText(Constants.notCountryOwner);
						continue;
					} else if (nodes[movingFromId].getUnits() < 2) {
						Console.appendText("This country does not have enough units!\n\n");
						continue;
					}
					break;
				}

				// Get country to which units will be moved.
				while (true) {
					movingTo = console.askUser("To which country do you want to move units?\n\n");
					movingToId = Constants.getCountryId(movingTo);

					if (movingToId == -1) {
						Console.appendText(Constants.countryDoesNotExist);
						continue;
					} else if (nodes[movingToId].getOwner() != player) {
						Console.appendText(Constants.notCountryOwner);
						continue;
					}
					break;
				}

				if (movingFromId == movingToId) {
					Console.appendText("You cannot choose the same country!\n\n");
					continue;
				}

				if (!nodes[movingFromId].isAdjacent(movingToId)) {
					Console.appendText("Countries must be adjacent!\n\n");
					continue;
				}

				if ((nodes[movingFromId].getUnits() - 1) == 0) {
					Console.appendText(
							"No units can be moved from this country," + "please choose different countries.\n\n");
					continue;
				}

				// Get number of units the player wishes to move.
				while (true) {
					Console.appendText("The maximum number of units that can be moved from this country is: "
							+ (nodes[movingFromId].getUnits() - 1) + "\n\n");
					numUnitsToMove = Integer.parseInt(console.askUser("How many units do you wish to move?\n\n"));
					if (numUnitsToMove >= nodes[movingFromId].getUnits()) {
						continue;
					}
					break;
				}
				break;
			}

			// Move units.
			nodes[movingFromId].setUnits(nodes[movingFromId].getUnits() - numUnitsToMove);
			nodes[movingToId].setUnits(nodes[movingToId].getUnits() + numUnitsToMove);
			refreshMap();
		}
	}

	private <T> boolean requestInput(String question, T answerOne, T answerTwo) {
		String answer = "";

		while (true) {
			answer = console.askUser(question + "\n\n");
			if (answer.equalsIgnoreCase((String) answerOne)) {
				return true;
			} else if (answer.equalsIgnoreCase((String) answerTwo)) {
				return false;
			}
		}
	}

	public void play() {
		setNamesAndColors();
		placeStartingUnits();

		for (int i = 0; getWinner() == -1; i = (i + 1) % players.length) {
			if (players[i].getCountriesOwned().size() > 0) {
				Console.appendText(players[i].getName() + " turn\n\n");
				placeReinforcements(players[i]);
				combat(players[i]);
				fortifyTerritory(players[i]);
			}
		}

		Console.appendText("Congratulations " + players[getWinner()].getName() + ", you have conquered the world\n");
	}

	public void setNamesAndColors() {
		List<Color> colors = new ArrayList<Color>(Arrays.asList(Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.ORANGE,
				Color.GREEN, Color.RED, Color.WHITE));
		List<String> possibleColors = new ArrayList<String>(
				Arrays.asList("CYAN", "YELLOW", "MAGENTA", "ORANGE", "GREEN", "RED", "WHITE"));
		for (int i = 0; i < players.length; i++) {
			if (!players[i].isPassive()) {
				players[i].setName(console.askUser("Please enter player " + (i + 1) + " name:\n").trim());
				refreshMap();
				Console.appendText("Possible choices for colors are:\n");
				for (int j = 0; j < possibleColors.size(); j++) {
					Console.appendText(possibleColors.get(j));
				}
				String color = "";
				do {
					color = console.askUser("Please enter color for player " + players[i].getName());
					color = color.toUpperCase();
					if (possibleColors.indexOf(color) != -1) {
						players[i].setColor(colors.get(possibleColors.indexOf(color)));
						int remove = possibleColors.indexOf(color.toUpperCase());
						possibleColors.remove(remove);
						colors.remove(remove);
						break;
					} else {
						Console.appendText("You entered wrong color\n");
					}
				} while (possibleColors.indexOf(color) == -1);

				Console.appendText("Drawing cards for " + players[i].getName() + ":\n");
				for (int j = 0; j < players[i].getCountriesOwned().size(); j++) {
					Console.appendText("Card: " + players[i].getCountriesOwned().get(j).toString());
				}
				refreshMap();
			} else {
				int randomColor = (int) (Math.random() * colors.size());
				String randomColorName = possibleColors.get(randomColor);
				randomColorName = randomColorName.toUpperCase();
				players[i].setColor(colors.get(possibleColors.indexOf(randomColorName)));
				possibleColors.remove(randomColor);
				colors.remove(randomColor);
				refreshMap();
			}
		}
	}

	public void refreshMap() {
		revalidate();
		repaint();
	}

	public static void main(String[] args) {
		// Create Frame
		JFrame frame = new JFrame("Risk");
		frame.setLayout(new BorderLayout());
		frame.getContentPane().setPreferredSize(new Dimension(
				Constants.FRAME_WIDTH + Constants.CONSOLE_WIDTH + Constants.BORDER_OFFSET, Constants.FRAME_HEIGHT));
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add Components
		Console console = new Console();
		RiskGame riskGame = new RiskGame(console);
		frame.add(riskGame, BorderLayout.WEST);
		frame.add(console, BorderLayout.EAST);

		// Display frame
		frame.setVisible(true);
		console.setCursor();
		frame.pack();

		riskGame.play();
	}
}