/*
 * Card.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

public class Card {
	private String territory;
	private int unit;
	private String insignia;

	public Card(String territory, int units, String insignia) {
		this.territory = territory;
		this.unit = units;
		this.insignia = insignia;
	}

	public String getTerritory() {
		return territory;
	}

	public int getUnit() {
		return unit;
	}

	public String getInsignia() {
		return insignia;
	}

	public boolean isWildCard() {
		return (insignia.equals("*WILDCARD*")) ? true : false;
	}

	public boolean isInfantryCard() {
		return (insignia.equals("Infantry")) ? true : false;
	}

	public boolean isCavalryCard() {
		return (insignia.equals("Cavalry")) ? true : false;
	}

	public boolean isArtilleryCard() {
		return (insignia.equals("Artillery")) ? true : false;
	}

	// Used for testing
	public String toString() {
		return territory + "-" + unit + "-" + insignia;
	}
}
