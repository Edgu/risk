/*
 * Player.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Player {
	private String name;
	private int units;
	private Color color;
	private boolean passive;
	private List<Card> cards;
	private List<Node> countriesOwned;

	public Player(String name, int units, Color colour, boolean passive) {
		this.name = name;
		this.units = units;
		this.color = colour;
		this.passive = passive;
		cards = new Vector<Card>();
		countriesOwned = new ArrayList<Node>();
	}

	public void addCountryOwned(Node node) {
		countriesOwned.add(node);
	}

	public void removeCountryOwned(Node node) {
		countriesOwned.remove(node);
	}

	public List<Node> getCountriesOwned() {
		return countriesOwned;
	}

	public Node getRandomCountry() {
		int randomIndex = (int) (Math.random() * countriesOwned.size());
		return countriesOwned.get(randomIndex);
	}

	public int amountOfControlledUnits() {
		int total = 0;
		for (int i = 0; i < countriesOwned.size(); i++) {
			total += countriesOwned.get(i).getUnits();
		}
		return total;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public Color getColour() {
		return color;
	}

	public boolean isPassive() {
		return passive;
	}

	public int rollDice() {
		return (int) (Math.random() * 6 + 1);
	}

	public void addCard(Card newCard) {
		cards.add(newCard);
	}

	public Card removeCard() {
		return (cards.isEmpty()) ? null : cards.remove(0);
	}

	public int numCards() {
		return cards.size();
	}

	public int amountOfWildCards() {
		int count = 0;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).isWildCard())
				count++;
		}
		return count;
	}

	public int amountOfInfantryCards() {
		int count = 0;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).isInfantryCard())
				count++;
		}
		return count;
	}

	public int amountOfCavalryCards() {
		int count = 0;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).isCavalryCard())
				count++;
		}
		return count;
	}

	public int amountOfArtilleryCards() {
		int count = 0;
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).isArtilleryCard())
				count++;
		}
		return count;
	}

	public int getReinforcementsForCards(String set, boolean unique) {
		int wildCards = 0;
		int infantryCards = 0;
		int cavalryCards = 0;
		int artilleryCards = 0;
		int wildToBeTaken = 0;

		wildCards = amountOfWildCards();
		infantryCards = amountOfInfantryCards();
		cavalryCards = amountOfCavalryCards();
		artilleryCards = amountOfArtilleryCards();

		System.out.println("Wild Cards: " + wildCards);
		System.out.println("Infantry Cards " + infantryCards);
		System.out.println("Cavalry Cards: " + cavalryCards);
		System.out.println("Artillery Cards: " + artilleryCards);

		// All letters are the same
		if (unique) {
			System.out.println("Unique");
			switch (set.charAt(0)) {
			case 'I':
				if (infantryCards < 3) {
					wildToBeTaken = 3 - infantryCards;
					if (wildToBeTaken > wildCards) {
						Console.appendText("You do not have enough Infantry and Wild Cards\n");
						return 0;
					}
				}
				break;
			case 'C':
				if (cavalryCards < 3) {
					wildToBeTaken = 3 - cavalryCards;
					if (wildToBeTaken > wildCards) {
						Console.appendText("You do not have enough Cavalry and Wild Cards\n");
						return 0;
					}
				}
				break;
			default:
				if (artilleryCards < 3) {
					wildToBeTaken = 3 - artilleryCards;
					if (wildToBeTaken > wildCards) {
						Console.appendText("You do not have enough Artillery and Wild Cards\n");
						return 0;
					}
				}
				break;
			}
		} else {
			System.out.println("Different");
			int inTotal = artilleryCards + infantryCards + cavalryCards;
			if (inTotal < 3) {
				wildToBeTaken = (3 - inTotal);
				if (wildToBeTaken > wildCards) {
					Console.appendText("You do not have enough Wild Cards\n");
					return 0;
				}
			}
		}

		// Removing wild cards from the deck
		for (int i = 0; i < cards.size() || wildToBeTaken > 0; i++) {
			if (cards.get(i).getInsignia().equals("*WILDCARD*")) {
				System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
				cards.remove(i);
				wildToBeTaken--;
				i = -1;
			}
		}

		// Removing different cards
		if (!unique) {
			int infantry = 1;
			int cavalry = 1;
			int artillery = 1;
			for (int i = 0; i < cards.size(); i++) {
				if (cards.get(i).getInsignia().equals("Infantry") && infantry != 0) {
					System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
					cards.remove(i);
					infantry--;
					i = -1;
					if (infantry == 0 && cavalry == 0 && artillery == 0) {
						break;
					}
				} else if (cards.get(i).getInsignia().equals("Cavalry") && cavalry != 0) {
					System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
					cards.remove(i);
					cavalry--;
					i = -1;
					if (infantry == 0 && cavalry == 0 && artillery == 0) {
						break;
					}
				} else if (cards.get(i).getInsignia().equals("Artillery") && artillery != 0) {
					System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
					cards.remove(i);
					artillery--;
					i = -1;
					if (infantry == 0 && cavalry == 0 && artillery == 0) {
						break;
					}
				}
			}
		} else {
			// Removing 3 of a kind
			int infantry = 0;
			int cavalry = 0;
			int artillery = 0;
			for (int i = 0; i < cards.size(); i++) {
				switch (set.charAt(0)) {
				case 'I':
					if (cards.get(i).getInsignia().equals("Infantry")) {
						System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
						cards.remove(i);
						infantry++;
						i = -1;
						if (infantry == 3) {
							break;
						}
					}
					break;
				case 'C':
					if (cards.get(i).getInsignia().equals("Cavalry")) {
						System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
						cards.remove(i);
						cavalry++;
						i = -1;
						if (cavalry == 3) {
							break;
						}
					}
					break;
				case 'A':
					if (cards.get(i).getInsignia().equals("Artillery")) {
						System.out.println("Index " + i + " Removed: " + cards.get(i).toString());
						cards.remove(i);
						artillery++;
						i = -1;
						if (artillery == 3) {
							break;
						}
					}
					break;
				}
			}
		}
		return Constants.calculateCardExchangeReinforcements();
	}

	public String printCards() {
		String text = "You have ";

		if (cards.size() == 0) {
			text += "no territory cards.\n";
		} else if (cards.size() == 1) {
			text = cards.size() + " card: \n";
		} else {
			text = cards.size() + " cards: \n";
		}

		for (int i = 0; i < cards.size(); i++) {
			text += cards.get(i) + "\n";
		}

		return text + "\n";
	}

	public void setAmountOfUnits(int units) {
		this.units = units;
	}

	public int getAmountOfUnits() {
		return units;
	}

	public int getNumberOfReinforcementsForCountries() {
		return (countriesOwned.size() < 9 ? 3 : countriesOwned.size() / 3);
	}

	public int getNumberOfReinforcementsForContinents() {
		int num = 0;
		int namerica = 0, europe = 0, asia = 0, australia = 0, samerica = 0, africa = 0;

		for (int i = 0; i < countriesOwned.size(); i++) {
			if (countriesOwned.get(i).getContinentId() == 0) {
				namerica++;
			} else if (countriesOwned.get(i).getContinentId() == 1) {
				europe++;
			} else if (countriesOwned.get(i).getContinentId() == 2) {
				asia++;
			} else if (countriesOwned.get(i).getContinentId() == 3) {
				australia++;
			} else if (countriesOwned.get(i).getContinentId() == 4) {
				samerica++;
			} else if (countriesOwned.get(i).getContinentId() == 5) {
				africa++;
			}
		}

		if (namerica == 9) {
			num += Constants.CONTINENT_VALUES[0];
		}
		if (europe == 7) {
			num += Constants.CONTINENT_VALUES[1];
		}
		if (asia == 12) {
			num += Constants.CONTINENT_VALUES[2];
		}
		if (australia == 4) {
			num += Constants.CONTINENT_VALUES[3];
		}
		if (samerica == 4) {
			num += Constants.CONTINENT_VALUES[4];
		}
		if (africa == 6) {
			num += Constants.CONTINENT_VALUES[5];
		}

		return num;
	}

	public int getTotalReinforcements() {
		return getNumberOfReinforcementsForCountries() + getNumberOfReinforcementsForContinents();
	}
}
