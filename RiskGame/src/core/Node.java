/*
 * Node.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

// Class which represents country
public class Node {
	private String name;
	private int[] position;
	private int[] adjacentNodes;
	private int continentId;
	private Color color;
	private int units;
	private Player owner;

	public boolean isAdjacent(int nodeId) {
		for (int n : adjacentNodes) {
			if (n == nodeId) {
				return true;
			}
		}
		return false;
	}

	public Node(String name, int[] adjacentNodes, int[] position, int continentId, Color color, Player owner) {
		this.name = name;
		this.adjacentNodes = adjacentNodes;
		this.position = position;
		this.continentId = continentId;
		this.color = color;
		this.owner = owner;
		units = 0;
	}

	// Draws circle, country name, number of units.
	public void draw(Graphics2D g2d) {
		g2d.setColor(color);
		g2d.fillOval(position[0], position[1], Constants.NODE_WIDTH, Constants.NODE_HEIGHT);
		g2d.setColor(Color.BLACK);
		g2d.drawOval(position[0], position[1], Constants.NODE_WIDTH, Constants.NODE_HEIGHT);
		int textWidth = g2d.getFontMetrics().stringWidth(String.valueOf(units));

		if (owner != null && owner.getColour() != null) {
			g2d.setColor(owner.getColour());
			g2d.setStroke(new BasicStroke(8));

			// Draw outer circle which indicates players color.
			g2d.drawOval(position[0] - ((Constants.OUTER_CIRCLE_WIDTH / 2) - Constants.NODE_WIDTH / 2),
					position[1] - ((Constants.OUTER_CIRCLE_HEIGHT / 2) - Constants.NODE_HEIGHT / 2),
					Constants.OUTER_CIRCLE_WIDTH, Constants.OUTER_CIRCLE_HEIGHT);
			g2d.setColor(Color.BLACK);
			g2d.setStroke(new BasicStroke(2));

			// Draw outlines of outer circles
			g2d.drawOval(position[0] - (((Constants.OUTER_CIRCLE_WIDTH + 4) / 2) - Constants.NODE_WIDTH / 2),
					position[1] - (((Constants.OUTER_CIRCLE_HEIGHT + 4) / 2) - Constants.NODE_HEIGHT / 2),
					Constants.OUTER_CIRCLE_WIDTH + 4, Constants.OUTER_CIRCLE_HEIGHT + 4);
			g2d.drawOval(position[0] - (((Constants.OUTER_CIRCLE_WIDTH - 4) / 2) - Constants.NODE_WIDTH / 2),
					position[1] - (((Constants.OUTER_CIRCLE_HEIGHT - 4) / 2) - Constants.NODE_HEIGHT / 2),
					Constants.OUTER_CIRCLE_WIDTH - 4, Constants.OUTER_CIRCLE_HEIGHT - 4);
			g2d.setColor(Color.WHITE);

			g2d.drawString(String.valueOf(units), 1 + position[0] + (Constants.NODE_WIDTH / 2) - (textWidth / 2F),
					position[1] + (Constants.NODE_HEIGHT / 2) + 5);
		}

		textWidth = g2d.getFontMetrics().stringWidth(name);

		// Draw outline of the country name.
		g2d.setColor(Color.WHITE);
		g2d.drawString(name, ((position[0] + (Constants.NODE_WIDTH / 2)) - (textWidth / 2F)) - 1,
				(position[1] - 15) - 1);
		g2d.drawString(name, ((position[0] + (Constants.NODE_WIDTH / 2)) - (textWidth / 2F)) - 1,
				(position[1] - 15) + 1);
		g2d.drawString(name, ((position[0] + (Constants.NODE_WIDTH / 2)) - (textWidth / 2F)) + 1,
				(position[1] - 15) - 1);
		g2d.drawString(name, ((position[0] + (Constants.NODE_WIDTH / 2)) - (textWidth / 2F)) + 1,
				(position[1] - 15) + 1);

		// Draw name.
		g2d.setColor(Color.BLACK);
		g2d.drawString(name, (position[0] + (Constants.NODE_WIDTH / 2)) - (textWidth / 2F), position[1] - 15);

	}

	public int getX() {
		return position[0];
	}

	public int getY() {
		return position[1];
	}

	public Color getColor() {
		return color;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String getName() {
		return name;
	}

	public int[] getAdjacentNodes() {
		return adjacentNodes;
	}

	public int getContinentId() {
		return continentId;
	}

	public int countAdjacentNodes() {
		return adjacentNodes.length;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public String toString() {
		return name + "(" + Constants.getCountryId(name) + ")";
	}
}
