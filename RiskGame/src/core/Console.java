/*
 * Console.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

@SuppressWarnings("serial")
public class Console extends JPanel implements ActionListener {
	private JLabel textLabel;
	private JButton textButton;
	private JTextField textField;
	private static JTextArea textArea;
	private LinkedList<String> buffer;
	private static String lastLine;
	private LinkedList<String> inputs;
	private static final int MAX_INPUTS = 10;

	public Console() {
		// Console Settings
		this.buffer = new LinkedList<String>();
		this.inputs = new LinkedList<String>();
		this.setPreferredSize(new Dimension(Constants.CONSOLE_WIDTH, Constants.FRAME_HEIGHT));
		this.setBackground(Color.black);

		// Text Field Settings
		textField = new JTextField(Constants.TEXT_FIELD_WIDTH);
		textField.addActionListener(this);
		textField.addKeyListener(new customKeyListener());
		textField.setBackground(Color.GRAY);

		// Text Area and Scroll Settings
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBackground(Color.GRAY);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(Constants.TEXT_AREA_WIDTH, Constants.TEXT_AREA_HEIGHT));
		DefaultCaret caret = (DefaultCaret) textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		// Label Settings
		textLabel = new JLabel("RISK Console");
		textLabel.setForeground(Color.white);

		// Button Settings
		textButton = new JButton("Enter");
		textButton.setPreferredSize(new Dimension(Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT));
		textButton.setFont(new Font("Dialog", Font.PLAIN, 11));
		textButton.addActionListener(this);
		textButton.setEnabled(true);

		// Add to Console panel
		this.add(textLabel);
		this.add(scrollPane);
		this.add(textField);
		this.add(textButton);
	}

	class customKeyListener implements KeyListener {
		int curr = 0;

		@Override
		public void keyPressed(KeyEvent k) {
			if (k.getKeyCode() == KeyEvent.VK_UP) {
				curr = (--curr > 0) ? curr : 0;
				setText();
			} else if (k.getKeyCode() == KeyEvent.VK_DOWN) {
				curr = (++curr < inputs.size()) ? curr : inputs.size() - 1;
				setText();
			} else if (k.getKeyCode() == KeyEvent.VK_ENTER) {
				curr = inputs.size() + 1;
			}
		}

		private void setText() {
			if (!inputs.isEmpty())
				textField.setText(inputs.get(curr));
			textField.requestFocus();
		}

		@Override
		public void keyReleased(KeyEvent k) {
		}

		@Override
		public void keyTyped(KeyEvent k) {
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String text = textField.getText();
		textArea.append("User:: " + text + "\n");
		textField.setText(null);

		// Add to previous inputs
		if (inputs.size() == MAX_INPUTS) {
			inputs.removeFirst();
		}
		inputs.add(text.trim());

		// If a Static Command
		if (text.startsWith("!")) {
			appendText(Commands.processStatic(text) + "\n" + lastLine);
		} else {
			synchronized (buffer) {
				buffer.add(text);
				buffer.notify();
			}
		}

		this.setCursor();
	}

	// Set cursor position on textField
	public void setCursor() {
		textField.requestFocusInWindow();

	}

	private String getCommand() {
		String text;

		synchronized (buffer) {
			while (buffer.isEmpty()) {
				try {
					buffer.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			text = buffer.pop();
		}

		return text;
	}

	public String askUser(String question) {
		lastLine = question;

		Console.appendText(question);
		return getCommand();
	}

	public static void appendText(String text) {
		if (!text.endsWith("\n"))
			text += "\n";

		textArea.append(text);
	}

	public static void appentText(Object object) {
		appendText(object.toString());
	}
}
