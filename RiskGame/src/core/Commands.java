/*
 * Commands.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

public class Commands {
	// All Commands with Definitions
	private static final String[][] COMMANDS = { { "exit", "Closes down the game immediately\n" },
			{ "commands", "Display all commands available except itself\n" },
			{ "help", "Gives a brief description about other commands\nCall it by: !help command\n" },
			{ "info", "Display information about a countries or continents\nCall it by: !info country/continent\n" },
			{ "short", "Display shortened name of a given country\nCall it by : !short country/continent\n" },
			// End of static commands (!)/Start of in-turn commands
			{ "skip", "End your turn\n" },
			{ "attack", "Attack country2 with units from country1\nCall it by: reinforce country1 country2 units\n" } };
	private static final int STATIC_COMMANDS = 5;

	// Main Variables
	private String command;
	private String arguments;

	// Argument Variables
	private int firstCountryID;
	private int secondCountryID;
	private int units;

	// Error Checking Variables
	private Boolean error;
	private String errorMessage;

	public Commands() {
		reset();
	}

	private static int getCommandIndex(String command) {
		int commandIndex = -1;
		for (int i = 0; i < COMMANDS.length; i++) {
			if (command.equalsIgnoreCase(COMMANDS[i][0])) {
				commandIndex = i;
			}
		}

		return commandIndex;
	}

	/***************** Non-Static Methods *****************/
	public void process(String input) {
		reset();

		// Split input into command and arguments
		int index = input.indexOf(" "), commandIndex;

		if (index != -1) {
			command = input.substring(0, index).trim();
			arguments = input.substring(index).trim();
		} else {
			command = input.trim();
			arguments = "";
		}
		commandIndex = getCommandIndex(command);

		// Process command and get the arguments
		if (commandIndex == 5) {
			// Skip

			arguments = "";
		} else if (commandIndex == 6 || commandIndex == 7) {
			// 2 words minimum, 6 words maximum with integers at the end
			String regex = "[a-zA-z]+ [a-zA-z]+ ?[a-zA-z]* ?[a-zA-z]* ?[a-zA-z]* ?[a-zA-z]* [0-9]+";

			if (!arguments.isEmpty() && arguments.matches(regex)) {
				String[] args = arguments.split(" ");
				String country = args[0];
				int i = 0;

				// Get attacking country
				while (i < args.length - 2 && (firstCountryID = Constants.getCountryId(country.trim())) == -1) {
					country += " " + args[++i];
				}

				if (i == args.length - 2) {
					errorMessage = "Unknown first country, " + country + ", has been entered";
				}

				// Get defending country
				country = "";
				while (i < args.length - 1 && (secondCountryID = Constants.getCountryId(country.trim())) == -1) {
					country += " " + args[++i];
				}

				if (i == args.length - 1) {
					errorMessage = "Unknown second country, " + country + ", has been entered";
				}

				// Get units
				units = Integer.parseInt(args[args.length - 1]);
				if (units <= 0) {
					errorMessage = "Units must be greater than 0";
				}
			} else {
				errorMessage = "Wrong format of arguments given: " + command + "country country units";
			}
		} else {
			errorMessage = "Unknown command: " + command;
		}

		// Set error
		error = !errorMessage.isEmpty();
	}

	private void reset() {
		command = "";
		arguments = "";
		firstCountryID = -1;
		secondCountryID = -1;
		units = -1;
		error = false;
		errorMessage = "";
	}

	public String getCommand() {
		return command;
	}

	public String getArguments() {
		return arguments;
	}

	public int getFirstCountryID() {
		return firstCountryID;
	}

	public String getFirstCountry() {
		return Constants.COUNTRY_NAMES[firstCountryID];
	}

	public int getSecondCountryID() {
		return secondCountryID;
	}

	public String getSecondCountry() {
		return Constants.COUNTRY_NAMES[secondCountryID];
	}

	public int getUnits() {
		return units;
	}

	public Boolean isError() {
		return error;
	}

	public String getError() {
		return errorMessage;
	}

	/***************** Static Methods *****************/
	public static String processStatic(String input) {
		// Split input into command and arguments
		int index = input.indexOf(" "), commandIndex = -1;
		String command = input.substring(1), arguments = "", text = "";

		if (index != -1) {
			command = input.substring(1, index).trim();
			arguments = input.substring(index).trim();
		}
		commandIndex = getCommandIndex(command);

		// Process command
		if (commandIndex == 0) {
			exit();
		} else if (commandIndex == 1) {
			text = commands();
		} else if (commandIndex == 2) {
			text = help(arguments);
		} else if (commandIndex == 3) {
			text = info(arguments);
		} else if (commandIndex == 4) {
			text = shortName(arguments);
		} else if (commandIndex >= STATIC_COMMANDS) {
			text = "\"" + command + "\" is not a static command, call it without the ! like so: "
					+ COMMANDS[commandIndex][0];
		} else {
			text = "The command \"" + command + "\" does not exist";
		}

		return text;
	}

	private static String commands() {
		String message = "\n";

		for (int i = 0; i < COMMANDS.length; i++) {
			if (i < STATIC_COMMANDS)
				message += "!";
			message += COMMANDS[i][0] + "\n";
		}

		return message + "\n";
	}

	private static void exit() {
		System.exit(0);
	}

	private static String help(String command) {
		String message = "";
		int index = getCommandIndex(command);

		if (index == -1) {
			message = "The command \"!" + command + "\" does not exist";
		} else {
			message = COMMANDS[index][0] + ": " + COMMANDS[index][1];
		}

		return "\n" + message + "\n";
	}

	private static String info(String name) {
		if (name.isEmpty()) {
			return "Expecting arguments but none given";
		}

		String message = "Displaying info about the ";
		int nameID;

		if ((nameID = Constants.getCountryId(name)) != -1) {
			// String is Country
			name = Constants.COUNTRY_NAMES[nameID];
			message += "country " + name + ": \n";

			// Continents
			message += "Continent: " + Constants.CONTINENT_NAMES[Constants.CONTINENT_IDS[nameID]] + "\n";

			// Countries
			message += Constants.ADJACENT[nameID].length + " Adjacent ";
			if (Constants.ADJACENT[nameID].length > 1) {
				message += "Countries:";
			} else {
				message += "Country:";
			}

			for (int i = 0; i < Constants.ADJACENT[nameID].length; i++) {
				message += "\n - " + Constants.COUNTRY_NAMES[Constants.ADJACENT[nameID][i]];
			}
		} else if ((nameID = Constants.getContinentID(name)) != -1) {
			// String is a continent
			name = Constants.CONTINENT_NAMES[nameID];
			message += "Continent " + name + ":\n";

			// Countries in the continent
			message += "Countries in " + name + " are:\n";
			for (int i = 0; i < Constants.NUM_COUNTRIES && Constants.CONTINENT_IDS[i] <= nameID; i++) {
				if (Constants.CONTINENT_IDS[i] == nameID) {
					message += "\n - " + Constants.COUNTRY_NAMES[i];
				}
			}
		} else {
			message = "Sorry but " + name + " is not a country nor a continent.\n";
		}
		return "\n" + message + "\n";

	}

	private static String shortName(String name) {
		int id;
		String message = "";

		if (name.isEmpty()) {
			return "Expecting arguments but none given";
		}

		if ((id = Constants.getCountryId(name)) != -1) {
			if (name.length() < 4) {
				message = "You just entered the shortened name of " + Constants.COUNTRY_NAMES[id];
			} else {
				name = Constants.COUNTRY_NAMES[id];
				message = "Shortened name for " + name + " is " + Constants.COUNTRY_SHORT_NAMES[id];
			}
		} else if ((id = Constants.getContinentID(name)) != -1) {
			if (name.length() < 4) {
				message = "You just entered the shortened name of " + Constants.CONTINENT_NAMES[id];
			} else {
				name = Constants.CONTINENT_NAMES[id];
				message = "Shortened name for " + name + " is " + Constants.CONTINENT_SHORT_NAMES[id];
			}
		}

		else {
			message = "Sorry, but this country/continent does not exit";
		}

		return "\n" + message + "\n";
	}
}
