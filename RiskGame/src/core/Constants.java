/*
 * Constants.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class Constants {
	public static final int NUM_PLAYERS = 2;
	public static final int NUM_NEUTRALS = 4;
	public static final int NUM_PLAYERS_PLUS_NEUTRALS = NUM_PLAYERS + NUM_NEUTRALS;
	public static final int NUM_COUNTRIES = 42;
	public static final int INIT_COUNTRIES_PLAYER = 9;
	public static final int INIT_COUNTRIES_NEUTRAL = 6;
	public static final int INIT_UNITS_PLAYER = 36;
	public static final int INIT_UNITS_NEUTRAL = 24;
	public static final int NODE_WIDTH = 24, NODE_HEIGHT = 24;
	public static final int OUTER_CIRCLE_WIDTH = 36, OUTER_CIRCLE_HEIGHT = 36;
	public static final int NUM_CONTINENTS = 6;
	public static final int FRAME_WIDTH = 1000;
	public static final int FRAME_HEIGHT = 600;
	public static final int TEXT_AREA_WIDTH = 275;
	public static final int TEXT_AREA_HEIGHT = 536;
	public static final int TEXT_FIELD_WIDTH = 19;
	public static final int CONSOLE_WIDTH = 300;
	public static final int BUTTON_WIDTH = 60;
	public static final int BUTTON_HEIGHT = 20;
	public static final int BORDER_OFFSET = 5;

	// Error Messages
	public static final String countryDoesNotExist = "This country does not exist!\n\n";
	public static final String notCountryOwner = "You do not own this country!\n\n";

	public static final String[] CONTINENT_NAMES = { "N America", "Europe", "Asia", "Australia", "S America",
			"Africa" };

	public static final String[] CONTINENT_SHORT_NAMES = { "NA", "EUR", "AS", "AUS", "SA", "AFR" };

	public static final int[] CONTINENT_IDS = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2,
			2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5 };

	public static final int[] CONTINENT_VALUES = { 5, 5, 7, 2, 2, 3 };

	public static final String[] COUNTRY_NAMES = { "Ontario", "Quebec", "NW Territory", "Alberta", "Greenland",
			"E United States", "W United States", "Central America", "Alaska", "Great Britain", "W Europe", "S Europe",
			"Ukraine", "N Europe", "Iceland", "Scandinavia", "Afghanistan", "India", "Middle East", "Japan", "Ural",
			"Yakutsk", "Kamchatka", "Siam", "Irkutsk", "Siberia", "Mongolia", "China", "E Australia", "New Guinea",
			"W Australia", "Indonesia", "Venezuela", "Peru", "Brazil", "Argentina", "Congo", "N Africa", "S Africa",
			"Egypt", "E Africa", "Madagascar" };

	public static final String[] COUNTRY_SHORT_NAMES = { "ON", "QC", "NWT", "AB", "GL", "EUS", "WUS", "CA", "AK", "GBR",
			"WEU", "SEU", "UKR", "NEU", "ISL", "SDN", "AFG", "IN", "ME", "JPN", "UL", "YKK", "KCK", "SM", "IKK", "SBR",
			"MNG", "CHN", "EAU", "NG", "WAU", "IND", "VEN", "PR", "BRA", "ARG", "COG", "NAF", "SAF", "EG", "EAF",
			"MDG" };

	public static final int[][] COUNTRY_COORD = { { 236, 166 }, { 305, 163 }, { 149, 108 }, { 166, 159 }, { 387, 65 },
			{ 246, 238 }, { 145, 221 }, { 189, 286 }, { 63, 107 }, { 411, 193 }, { 405, 254 }, { 469, 246 },
			{ 566, 156 }, { 483, 190 }, { 414, 131 }, { 489, 107 }, { 637, 216 }, { 677, 313 }, { 565, 286 },
			{ 874, 234 }, { 656, 148 }, { 831, 105 }, { 907, 111 }, { 762, 329 }, { 787, 158 }, { 716, 110 },
			{ 799, 214 }, { 724, 264 }, { 874, 485 }, { 877, 402 }, { 766, 486 }, { 765, 405 }, { 212, 344 },
			{ 231, 412 }, { 302, 393 }, { 238, 489 }, { 490, 421 }, { 411, 348 }, { 480, 529 }, { 500, 314 },
			{ 542, 387 }, { 567, 494 } };

	public static final int[][] ADJACENT = { { 4, 1, 5, 6, 3, 2 }, { 4, 5, 0 }, { 4, 0, 3, 8 }, { 2, 0, 6, 8 },
			{ 14, 1, 0, 2 }, { 0, 1, 7, 6 }, { 3, 0, 5, 7 }, { 6, 5, 32 }, { 2, 3, 22 }, { 14, 15, 13, 10 },
			{ 9, 13, 11, 37 }, { 13, 12, 18, 39, 10 }, { 20, 16, 18, 11, 13, 15 }, { 15, 12, 11, 10, 9 }, { 15, 9, 4 },
			{ 12, 13, 14 }, { 20, 27, 17, 18, 12 }, { 16, 27, 23, 18 }, { 12, 16, 17, 40, 39, 11 }, { 26, 22 },
			{ 25, 27, 16, 12 }, { 22, 24, 25 }, { 8, 19, 26, 24, 21 }, { 27, 31, 17 }, { 21, 22, 26, 25 },
			{ 21, 24, 26, 27, 20 }, { 24, 22, 19, 27, 25 }, { 26, 23, 17, 16, 20, 25 }, { 29, 30 }, { 28, 30, 31 },
			{ 29, 28, 31 }, { 23, 29, 30 }, { 7, 34, 33 }, { 32, 34, 35 }, { 32, 37, 35, 33 }, { 33, 34 },
			{ 37, 40, 38 }, { 10, 11, 39, 40, 36, 34 }, { 36, 40, 41 }, { 11, 18, 40, 37 }, { 39, 18, 41, 38, 36, 37 },
			{ 38, 40 } };
	public static final int[] COUNTRY_CARD_UNITS = { 2, 3, 3, 1, 2, 3, 1, 2, 1, 2, 1, 2, 3, 2, 1, 3, 1, 1, 3, 1, 2, 2,
			2, 3, 1, 3, 3, 2, 1, 2, 3, 2, 3, 2, 3, 1, 2, 1, 3, 1, 3, 1 };
	public static final String[] CARD_INSIGNIAS = { "Wildcards", "Infantry", "Cavalry", "Artillery" };
	public static final int CARD_MAX = 5;
	public static final int CARD_NO_WILDCARDS = 3;
	private static int CARD_SETS_HANDED_IN = 0;

	public static int getCountry(MouseEvent e) {
		int x1, y1, x2 = e.getX() - 12, y2 = e.getY() - 12, index = -1;

		for (int i = 0; i < Constants.NUM_COUNTRIES && index == -1; i++) {
			x1 = Constants.COUNTRY_COORD[i][0];
			y1 = Constants.COUNTRY_COORD[i][1];

			if ((int) Point2D.distance(x1, y1, x2, y2) <= Constants.OUTER_CIRCLE_HEIGHT) {
				index = i;
			}
		}
		return index;
	}

	public static int getCountryId(String countryName) {
		for (int i = 0; i < NUM_COUNTRIES; i++) {
			if (countryName.equalsIgnoreCase(COUNTRY_NAMES[i])
					|| countryName.equalsIgnoreCase(COUNTRY_SHORT_NAMES[i])) {
				return i;
			}
		}
		return -1;
	}

	public static int getContinentID(String continentName) {
		for (int i = 0; i < NUM_CONTINENTS; i++) {
			if (continentName.equalsIgnoreCase(CONTINENT_NAMES[i])
					|| continentName.equalsIgnoreCase(CONTINENT_SHORT_NAMES[i])) {
				return i;
			}
		}
		return -1;
	}

	public static Color getContinentColor(int countryID) {
		switch (CONTINENT_IDS[countryID]) {
		case 0:
			return new Color(180, 150, 0);
		case 1:
			return new Color(0, 150, 150);
		case 2:
			return new Color(0, 200, 50);
		case 3:
			return new Color(150, 0, 100);
		case 4:
			return Color.RED;
		case 5:
			return new Color(170, 80, 0);
		default:
			return null;
		}
	}

	public static int calculateCardExchangeReinforcements() {
		// 4 6 8 10 12 15 20 25 30 35 40 45 50 55 60
		return (++CARD_SETS_HANDED_IN < 6) ? 2 + (2 * CARD_SETS_HANDED_IN) : 10 + (5 * (CARD_SETS_HANDED_IN - 5));
	}
}