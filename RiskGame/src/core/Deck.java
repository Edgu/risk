/*
 * Deck.java
 * Student Names: Edgaras Lagunovas, Mantas Ciutys, Stephen Patton
 * Student Numbers: 15204377, 14720511, 14342876
 */

package core;

import java.util.Collections;
import java.util.Vector;

public class Deck {
	private Vector<Card> deck;

	public Deck() {
		deck = new Vector<Card>(Constants.NUM_COUNTRIES + Constants.CARD_NO_WILDCARDS);
		this.reset();
	}

	public void shuffle() {
		Collections.shuffle(deck);
	}

	public void reset() {
		if (deck.size() > 0)
			deck.clear();

		for (int i = 0; i < Constants.NUM_COUNTRIES; i++) {
			String country = Constants.COUNTRY_NAMES[i];
			int units = Constants.COUNTRY_CARD_UNITS[i];
			String insignia = Constants.CARD_INSIGNIAS[Constants.COUNTRY_CARD_UNITS[i]];

			deck.add(i, new Card(country, units, insignia));
		}

		this.shuffle();
	}

	public void addWildCards() {
		for (int i = 0; i < Constants.CARD_NO_WILDCARDS; i++) {
			deck.add(i + Constants.NUM_COUNTRIES, new Card("", 0, "*WILDCARD*"));
		}

		this.shuffle();
	}

	public boolean containsWildCards() {
		return deck.contains(new Card("", 0, "*WILDCARD*"));
	}

	public Card pop() {
		Card tmp = deck.elementAt(0);
		deck.remove(0);
		return tmp;
	}

	public void push(Card elem) {
		deck.add(elem);
	}

	public int size() {
		return deck.size();
	}

	public boolean isEmpty() {
		return deck.isEmpty();
	}

	public String toString() {
		String text = deck.size() + " :";

		for (int i = 0; i < deck.size(); i++) {
			text += deck.elementAt(i) + "\n";
		}

		return text;
	}
}
