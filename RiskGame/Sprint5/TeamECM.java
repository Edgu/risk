import java.util.ArrayList;
import java.util.List;

// put your code here

public class TeamECM implements Bot {
	// The public API of YourTeamName must not change
	// You cannot change any other classes
	// YourTeamName may not alter the state of the board or the player objects
	// It may only inspect the state of the board and the player objects
	// So you can use player.getNumUnits() but you can't use
	// player.addUnits(10000), for example

	private BoardAPI board;
	private PlayerAPI player;
	// Priority to put reinforcements: Australia, S America, N America,
	// Asia, Europe, Africa
	private static final int[] PRIORITIES = { 3, 4, 0, 2, 1, 5 };
	// Entry points for every continent
	private static final int[][] ENTRIES = { { 16, 18, 20, 22, 23 }, { 10, 11, 12, 14 }, { 31 }, { 4, 7, 8 }, { 32, 34 },
			{ 10, 11, 12, 14 } };
	private static int name = 0;

	TeamECM(BoardAPI inBoard, PlayerAPI inPlayer) {
		board = inBoard;
		player = inPlayer;
	}

	public String getName() {
		String command = "";
		command = "BOT" + name;
		name++;
		return (command);
	}

	public String getReinforcement() {
		String command = "";
		int counter = 0;
		int[] aroundEnemies = new int[GameData.NUM_COUNTRIES];

		continents: for (int i = 0; i < PRIORITIES.length; i++) {
			int continent = PRIORITIES[i];

			// If continent is taken, do not put any reinforcements there
			if (takenContinent(GameData.CONTINENT_COUNTRIES[continent])) {
				continue;
			}

			for (int j = 0; j < GameData.CONTINENT_COUNTRIES[continent].length; j++) {

				int country = GameData.CONTINENT_COUNTRIES[continent][j];

				// If player controls this country
				if (board.getOccupier(country) == player.getId()) {

					// If there are adjacent enemies of this country
					if (areAdjacentEnemies(country)) {

						// Put an enemy into an array
						aroundEnemies[counter++] = country;

						// Finding average amount of units in one country for a
						// bot
						int numberOfPlayersUnits = numberOfUnits(player);
						int controlledCountries = numberOfControlledCountries(player);
						int average = Math.round(numberOfPlayersUnits / controlledCountries);

						if (board.getNumUnits(country) <= average * 2) {
							command = GameData.COUNTRY_NAMES[country];
							break continents;
						}
					}

				}

			}

		}
		// If command is empty, get a random country from the array of
		// countries that have enemies around them
		if (command == "") {
			int randomCountry = aroundEnemies[(int) (Math.random() * counter)];
			command = GameData.COUNTRY_NAMES[randomCountry];
		}

		// This should never be executed
		if (command == "") {
			command = GameData.COUNTRY_NAMES[(int) (Math.random() * GameData.NUM_COUNTRIES)];
			System.out.println("RANDOM USED :" + player.getName());
		}

		command = command.replaceAll("\\s", "");
		command += " 1";
		return (command);

	}

	public String getPlacement(int forPlayer) {
		String command = "";
		boolean owns = false;
		do {
			int randomCountry = (int) (Math.random() * GameData.NUM_COUNTRIES);
			if (board.getOccupier(randomCountry) == forPlayer) {
				if (areAdjacentEnemies(forPlayer)) {
					command = GameData.COUNTRY_NAMES[randomCountry];
					owns = true;
				}
			}
		} while (!owns);
		command = command.replaceAll("\\s", "");
		return (command);
	}

	public String getCardExchange() {
		String command = "";

		ArrayList<Card> cards = player.getCards();
		int cavalry = 0;
		int infantry = 0;
		int artillery = 0;
		int wildCard = 0;

		if (cards.size() < 5) {
			command = "skip";
		} else {
			for (int i = 0; i < cards.size(); i++) {
				switch (cards.get(i).getInsigniaId()) {
				case 0:
					infantry++;
					break;
				case 1:
					cavalry++;
					break;
				case 2:
					artillery++;
					break;
				case 3:
					wildCard++;
					break;
				}
			}

			if (cavalry >= 3 || infantry >= 3 || artillery >= 3 || wildCard >= 3) {
				if (infantry >= 3)
					command = "III";
				else if (cavalry >= 3)
					command = "CCC";
				else if (artillery >= 3)
					command = "AAA";
				else
					command = "WWW";

			} else {
				if (infantry >= 1 && artillery >= 1 && cavalry >= 1)
					command = "IAC";
				if (wildCard >= 1 && artillery >= 1 && cavalry >= 1)
					command = "CAW";
				if (infantry >= 1 && artillery >= 1 && wildCard >= 1)
					command = "IAW";
				if (infantry >= 1 && wildCard >= 1 && cavalry >= 1)
					command = "ICW";

				if (cavalry == 2) {
					if (wildCard == 1)
						command = "CCW";
				} else if (cavalry == 1) {
					if (wildCard == 2)
						command = "CWW";
				}

				if (infantry == 2) {
					if (wildCard == 1)
						command = "IIW";
				} else if (infantry == 1) {
					if (wildCard == 2)
						command = "IWW";
				}

				if (artillery == 2) {
					if (wildCard == 1)
						command = "AAW";
				} else if (artillery == 1) {
					if (wildCard == 2)
						command = "AWW";
				}
			}
		}
		return (command);
	}

	public String getBattle() {
		String command = "";
		// put your code here

		continents: for (int i = 0; i < PRIORITIES.length; i++) {
			int continent = PRIORITIES[i];

			// Looping through every country in a continent
			for (int j = 0; j < GameData.CONTINENT_COUNTRIES[continent].length; j++) {
				int country = GameData.CONTINENT_COUNTRIES[continent][j];

				// If that country is owned by bot and has more than 2 units in
				// it, attack from it
				if (board.getOccupier(country) == player.getId() && board.getNumUnits(country) > 2) {

					int attacker = country;
					String attackerCountry = GameData.COUNTRY_NAMES[attacker].replaceAll("\\s+", "");

					// Find out which adjacent has least amount of units and
					// attack that neighbour
					int attacked = minNeighbour(attacker);
					if (attacked == -1) {
						continue;
					}

					String attackedCountry = GameData.COUNTRY_NAMES[attacked].replaceAll("\\s+", "");

					// Determining the amount of units used in attack
					int units = 0;
					if (board.getNumUnits(attacker) > 3) {
						units = 3;
					} else if (board.getNumUnits(attacker) == 3) {
						units = 2;
					} else {
						units = 1;
					}

					command = attackerCountry + " " + attackedCountry + " " + units;
					break continents;
				}
			}
		}
		if (command == "") {
			command = "skip";
		}
		return (command);
	}

	public String getDefence(int countryId) {
		String command = "";
		int defence = board.getNumUnits(countryId);
		if (defence > 1)
			command = "2";
		else
			command = "1";
		return (command);
	}

	public String getMoveIn(int attackCountryId) {
		String command = "";

		int units = board.getNumUnits(attackCountryId);

		switch (units) {
		case 1:
			command = String.valueOf("0");
			break;
		case 3:
			command = String.valueOf("1");
			break;
		default:
			if (isEntryNode(attackCountryId)) {
				command = String.valueOf(units - 1);
			} else {
				command = String.valueOf((int) units / 2);
			}
			break;
		}

		return (command);
	}

	public String getFortify() {
		String command = "";

		List<Integer> ownCountries = getOwnedCountries();
		List<Integer> ownEntryNodes = getOwnedEntryCountries();
		List<Integer> ownEntryPriorityNodes = new ArrayList<Integer>();


		// 2. Sort own entry nodes by priority
		for (int i = 0; i < PRIORITIES.length; i++) {
			for (int j = 0; j < GameData.CONTINENT_COUNTRIES[PRIORITIES[i]].length; j++) {
				for (int k = 0; k < ownEntryNodes.size(); k++) {
					if (GameData.CONTINENT_COUNTRIES[PRIORITIES[i]][j] == ownEntryNodes.get(k)) {
						ownEntryPriorityNodes.add(ownEntryNodes.get(k));
						ownEntryNodes.remove(k);
						k--;
					}
				}
			}
		}

		System.out.println(ownCountries);
		System.out.println(ownEntryNodes);
		System.out.println(ownEntryPriorityNodes);

		// 3. fortify random country if no priority nodes available
		if (ownEntryPriorityNodes.size() == 0) {
			int randomCountry = (int) (Math.random() * ownCountries.size());
			ownEntryPriorityNodes.add(ownCountries.get(randomCountry));
		}

		int randomCountry = (int) (Math.random() * ownEntryPriorityNodes.size());
		List<Integer> ownAdjacentNodes = getAdjacentNodes(ownEntryPriorityNodes.get(randomCountry));
		System.out.println(ownAdjacentNodes);
		System.out.println();

		int max = 0;
		int country = -1;

		for (int i = 0; i < ownAdjacentNodes.size(); i++) {
			if (board.getNumUnits(ownAdjacentNodes.get(i)) > max && board.getOccupier(ownAdjacentNodes.get(i)) == player.getId()) {
				max = board.getNumUnits(ownAdjacentNodes.get(i));
				country = ownAdjacentNodes.get(i);
			}
		}

		if (max > 1) {
			max--;
			command = GameData.COUNTRY_NAMES[country].replaceAll("\\s", "") + " " + GameData.COUNTRY_NAMES[ownEntryPriorityNodes.get(randomCountry)].replaceAll("\\s", "") + " " + max;
			System.out.println(command);
		} else {
			command = "skip";
		}

		return (command);
	}

	private boolean isEntryNode(int countryId) {
		for (int i = 0; i < ENTRIES.length; i++) {
			for (int j = 0; j < ENTRIES[i].length; j++) {
				if (ENTRIES[i][j] == countryId) {
					return true;
				}
			}
		}
		return false;
	}

	private List<Integer> getAdjacentNodes(int countryId) {
		List<Integer> adjacentNodes = new ArrayList<Integer>();
		for (int i = 0; i < GameData.NUM_COUNTRIES; i++) {
			if (board.isAdjacent(countryId, i)) {
				adjacentNodes.add(i);
			}
		}
		return adjacentNodes;
	}

	private List<Integer> getOwnedCountries() {
		List<Integer> ownedCountries = new ArrayList<Integer>();
		for (int i = 0; i < GameData.NUM_COUNTRIES; i++) {
			if (board.getOccupier(i) == player.getId()) {
				ownedCountries.add(i);
			}
		}
		return ownedCountries;
	}

	private List<Integer> getOwnedEntryCountries() {
		List<Integer> ownedCountries = getOwnedCountries();
		List<Integer> ownedEntry = new ArrayList<Integer>();

		for (int i = 0; i < ENTRIES.length; i++) {
			for (int j = 0; j < ENTRIES[i].length; j++) {
				int entryCountry = ENTRIES[i][j];
				for (int k = 0; k < ownedCountries.size(); k++) {
					if (ownedCountries.get(k) == entryCountry) {
						ownedEntry.add(entryCountry);
						ownedCountries.remove(k);
						break;
					}
				}
			}
		}
		return ownedEntry;
	}

	// Method to find out whether a continent is fully conquered or not
	private boolean takenContinent(int[] countries) {
		boolean taken = true;
		for (int i = 0; i < countries.length; i++) {
			if (board.getOccupier(countries[i]) != player.getId()) {
				taken = false;
				break;
			}
		}
		return taken;
	}

	// Method to find out which adjacent country has least amount of units
	private int minNeighbour(int countryId) {
		int neighbour = -1;
		int min = board.getNumUnits(GameData.ADJACENT[countryId][0]);

		for (int i = 0; i < GameData.ADJACENT[countryId].length; i++) {
			int countryNeighbour = GameData.ADJACENT[countryId][i];
			if (board.getNumUnits(countryNeighbour) < min) {
				if (board.getOccupier(countryNeighbour) != board.getOccupier(countryId)) {
					min = board.getNumUnits(countryNeighbour);
					neighbour = countryNeighbour;
				}
			}
		}
		return neighbour;
	}

	// Method to find out whether the country has adjacent enemies or not
	private boolean areAdjacentEnemies(int attacker) {
		boolean enemy = false;
		for (int i = 0; i < GameData.ADJACENT[attacker].length; i++) {
			int adjacent = GameData.ADJACENT[attacker][i];
			if (board.getOccupier(attacker) != board.getOccupier(adjacent)) {
				enemy = true;
			}
		}
		return enemy;
	}

	// Method to find out how many units in total a bot has
	private int numberOfUnits(PlayerAPI player) {
		int sum = 0;
		for (int i = 0; i < GameData.COUNTRY_NAMES.length; i++) {
			if (board.getOccupier(i) == player.getId()) {
				sum += board.getNumUnits(i);
			}
		}
		return sum;
	}

	// Method to find out how many countries a bot controls
	private int numberOfControlledCountries(PlayerAPI player) {
		int count = 0;
		for (int i = 0; i < GameData.COUNTRY_NAMES.length; i++) {
			if (board.getOccupier(i) == player.getId()) {
				count++;
			}
		}
		return count;
	}

}
